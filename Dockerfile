# Extend from node official image:
FROM node:13.11

# Create app directory
WORKDIR /usr/src/app

# Bundle app source
COPY ./ ./
COPY package*.json ./

# Expose correct port
EXPOSE 3000

RUN npm install --only=production
CMD [ "npm", "run", "start" ]