const request = require('supertest')
const app = require('../src/server')
let cache = {}

describe('Customer process', () => {
    test('should create a new customer', async () => {
      const res = await request(app)
        .post('/customer')
        .send({
          "name": "delboni",
          "email": "rua itaituba",
          "phone": "954068189",
          "cpf": "44661357892"
      })
      cache.testId = res.body._id
      expect(res.statusCode).toEqual(201 || 200)
    })

    test('should get previously created customer', async () => {
      const res = await request(app)
        .get(`/customer/${cache.testId}`)

      expect(res.body._id).toEqual(cache.testId)
    })

    test('should edit previously created customer', async () => {
      cache.editedCustomer = {
        _id: cache.testId,
        name: "Matheus",
        email: "rua amaral gurgel",
        phone: "954068189",
        cpf: "44661357892"
      }
      const res = await request(app)
        .put(`/customer/${cache.testId}`)
        .send(cache.editedCustomer)

      console.log(res.body)
      expect(res.body).toMatchObject(cache.editedCustomer)
    })

    test('should deactivate test customer', async () => {
      const res = await request(app)
        .delete(`/customer/${cache.testId}`)

      expect(res.body._id).toEqual(cache.testId)
      expect(res.body.active).toBeFalsy()
    })
})

