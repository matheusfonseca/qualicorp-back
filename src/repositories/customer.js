const Customer = require('../models/customer')

const customerRepo = () => {
  const findCustomerByText = (partialText) => {
    console.log(partialText)
    return Customer.find({ name: { $regex: `${partialText}`, $options: "i" } }, (err, docs) => {
      console.log(err)
      return docs
      });
  }

  const addCustomer = (customerInfo) => {
    const customer = new Customer(customerInfo)

    try {
      const result = customer.save()

      return result
    } catch (err) {
      console.log(err)
      throw err
    }
  }

  const findCustomer = async (params) => {
    try {
      const result = await Customer.find(params)

      return result
    } catch (err) {
      console.log(err)
      throw err
    }
  }

  const addCustomers = async (customerArray) => {
    try {
      const result = await Customer.insertMany(customerArray)

      return result
    } catch (err) {
      console.log(err)
      throw err
    }

  }

  const getCustomer = async (id) => {
    try {
      const result = await Customer.findById(id)

      return result
    } catch (err) {
      console.log(err)
      throw err
    }
  }

  const updateCustomer = async (customer) => {
    try {
      const result = await Customer.findByIdAndUpdate(customer._id, customer, { new: true })

      return result
    } catch (err) {
      console.log(err)
      throw err
    }
  }

  const removeCustomer = async (customerId) => {
    try {
      const result = await Customer.deleteOne({ "_id": customerId })

      return result
    } catch (err) {
      console.log(err)
      throw err
    }
  }

  return {
    addCustomer,
    addCustomers,
    getCustomer,
    findCustomer,
    findCustomerByText,
    updateCustomer,
    removeCustomer
  }
}

module.exports = customerRepo
