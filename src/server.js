require('dotenv').config()

const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')

const customerController = require('./controllers/customer')

const app = express()

app.use(bodyParser.json())
app.use(cors())

app.use('/customer', customerController())

module.exports = app
