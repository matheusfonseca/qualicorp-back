const mongoose = require('mongoose')
const db_uri = process.env.DB_URI

module.exports = mongoose.createConnection(db_uri, {useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false})
