const { Router } = require('express')

const customerRepo = require('../repositories/customer')

const { addCustomer, getCustomer, findCustomer, updateCustomer, removeCustomer } = customerRepo()

const customerController = () => {
  const findByText = async (req, res) => {
    try {
      const customerName = req.query.name
      const result = await findCustomerByText(customerName)
  
      res.send(result)
    } catch (err) {
      res.status(400).send(err)
    }
  }

  const find = async (req, res) => {
    try {
      const query = req.query
      const result = await findCustomer({ ...query })
  
      res.send(result)
    } catch (err) {
      console.log(err)
      res.status(400).send(err)
    }
  }

  const get = async (req, res) => {
    try {
      const id = req.params.id
      const result = await getCustomer(id)
  
      res.send(result)
    } catch (err) {
      res.status(400).send(err)
    }
  }

  const create = async (req, res) => {
    try {
      const newLab = req.body
      const result = await addCustomer(newLab)
  
      res.status(201).send(result)
    } catch (err) {
      res.status(400).send(err)
    }
  }

  const update = async (req, res) => {
    try {
      const updatedCustomer = {
        _id: req.params.id,
        ...req.body
      }
      const result = await updateCustomer(updatedCustomer)

      res.status(200).send(result)
    } catch (err) {
      res.status(400).send(err)
    }
  }

  const remove = async (req, res) => {
    try {
      const id = req.params.id
      const result = await removeCustomer(id)
  
      res.send(result)
    } catch (err) {
      res.status(400).send(err)
    }
  }

  const router = Router()

  router.get('/', find)
  router.get('/text/', findByText)
  router.get('/:id', get)
  router.post('/', create)
  router.put('/:id', update)
  router.delete('/:id', remove)

  return router
}

module.exports = customerController
