const mongoose = require('../util/dbConnection')
const { Schema } = require('mongoose')

const customerSchema = new Schema({
  name:  {
    type: String,
    required: true
  },
  email:  {
    type: String,
    required: true
  },
  phone: {
    type: String, 
    required: true,
    validate: {
      validator: (v) => {
        return /\d{9}/.test(v);
      },
      message: props => `${props.value} not a valid phone number`
    }
  },
  cpf: {
    type: String, 
    required: true,
    validate: {
      validator: (cpf) => {
        let sum, remainder;
        sum = 0;
      
        if (cpf == "00000000000") return false
           
        for (i=1; i<=9; i++) sum = sum + parseInt(cpf.substring(i-1, i)) * (11 - i)

        remainder = (sum * 10) % 11
        
        if ((remainder == 10) || (remainder == 11))  remainder = 0
        if (remainder != parseInt(cpf.substring(9, 10)) ) return false
        
        sum = 0

        for (i = 1; i <= 10; i++) sum = sum + parseInt(cpf.substring(i-1, i)) * (12 - i)

        remainder = (sum * 10) % 11
        
        if ((remainder == 10) || (remainder == 11))  remainder = 0
        if (remainder != parseInt(cpf.substring(10, 11) ) ) return false

        return true
      },
      message: props => `${props.value} not a valid cpf number`,
    }
  }
})

const customerModel = mongoose.model('Customer', customerSchema)

module.exports = customerModel
