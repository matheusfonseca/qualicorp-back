## Table of contents
* [Arquitetura](#arquitetura)
* [Tecnologias](#tecnologias)
* [Instruções](#instruções)

## Arquitetura

Dividi a aplicação em:

  * App:
    * No app fica o roteamento das chamadas para os controllers corretos. 
  * Controllers:
    * Fica a lógica de negócios e a responsabilidade de executar as operações dos repositórios.
  * Repositories:
    * Nos repositórios ficam todas as interações com o banco de dados. Esse acesso, no caso do mongo, se dá através das _Models_, que define a estrutura dos documentos.
  * Util:
    * Aqui ficam as utilidades e operações reaproveitaveis em diferentes partes do projeto.

## Tecnologias
* Node.js
* Express
* MongoDB
* Mongoose
* Jest
* Swagger


## Instruções
O serviço está disponível na URL: https://quali-back.herokuapp.com/ e a documentação está disponível [aqui](https://quali-back.herokuapp.com/api-docs)

Para rodar localmente:

```
$ git clone https://bitbucket.org/matheusfonseca/quali-back.git
$ cd dasa-lab-back
$ npm install
$ npm start
```

## todo
- error handling no nivel do repositório
- terminar os testes
- terminar documentação